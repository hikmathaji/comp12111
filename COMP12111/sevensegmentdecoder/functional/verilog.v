//Verilog HDL for "COMP12111", "sevensegmentdecoder" "functional"


module sevensegmentdecoder (input [3:0] bcd,
			    output reg [7:0] segments);
	
always @ (bcd)
	case(bcd)
	0:	segments = 8'b0011_1111;
	1:	segments = 8'b0000_0110;
	2:	segments = 8'b0101_1011;
	3:	segments = 8'b0100_1111;
	4:	segments = 8'b0110_0110;
	5:	segments = 8'b0110_1101;
	6:	segments = 8'b0111_1101;
	7:	segments = 8'b0000_0111;
	8:	segments = 8'b0111_1111;
	9:	segments = 8'b0110_1111;
	
	10:	segments = 8'b0111_0111;
	11:	segments = 8'b0111_1100;
	12:	segments = 8'b0011_1001;
	13:	segments = 8'b0101_1110;
	14:	segments = 8'b0111_1001;
	15:	segments = 8'b0111_0001;
	endcase
endmodule
