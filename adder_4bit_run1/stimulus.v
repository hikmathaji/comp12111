// Verilog stimulus file.
// Please do not create a module in this file.
/*

#VALUE      creates a delay of VALUE ns
a=VALUE;    sets the value of input 'a' to VALUE
$stop;      tells the simulator to stop

*/

initial
begin
#100
// Enter you stimulus below this line
// Using 14 test vectors the interconnect of the 4bit added can be verified 

// Check all fulladders are connected to something, S=0 cout=0 not Xs
a=0; b=0; cin =0;
//Check connections for A[0], B[0], Cin, S[0], S=0001 cout=0
#100 a='b0001; b='b0000; 
#100 a='b0000; b='b0001; 
#100 a='b0000; b='b0000; cin=1;
//Check connection of carry out of the first adder
#100 a='b0001; b='b0001; cin=0;
//Check connections for A[1], B[1], S[1]
#100 a='b0010; b='b0000;
#100 a='b0000; b='b0010; 
//ADD 7 MORE TESTS TO COMPLETE CONNECTIVITY TESTS //

//Check connections for A[2], B[2], S[2]
#100 a='b0100; b='b0000; 
#100 a='b0000; b='b0100;

//Check connections for A[3], B[3], S[3]
#100 a='b1000; b='b0000; 
#100 a='b0000; b='b1000; 

//Check carry out from the second adder
#100 a='b0010; b='b0010; 

//Check carry out from the third adder
#100 a='b0100; b='b0100;

//Check carry out from the fourth adder
#100 a='b1000; b='b1000;

// ADD TESTS FOR FINDING CARRY DELAY HERE //

#100 a='b1111; b='b0000; cin=1;
// delay for end of wave traces to be visible
#100

// Please make sure your stimulus is above this line
$stop;
end
