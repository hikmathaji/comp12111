
// Verilog stimulus file.
// Please do not create a module in this file.
/*

#VALUE      creates a delay of VALUE ns
a=VALUE;    sets the value of input 'a' to VALUE
$stop;      tells the simulator to stop

*/

initial
begin

// Enter you stimulus below this line

a = 16'h0FFF;
b = 16'h0000;
sel = 1;
#100
sel = 0;
#100


// Please make sure your stimulus is above this line 
$stop;
end 
