// Verilog stimulus file.
// Please do not create a module in this file.
/*

#VALUE      creates a delay of VALUE ns
a=VALUE;    sets the value of input 'a' to VALUE
$stop;      tells the simulator to stop

*/

initial
begin
#100 
// Enter you stimulus below this line
bcd = 0;//0
#100 bcd = 'b0001;//1
#100 bcd = 'b0010;//2
#100 bcd = 'b0011;//3
#100 bcd = 'b0100;//4
#100 bcd = 'b0101;//5
#100 bcd = 'b0110;//6
#100 bcd = 'b0111;//7
#100 bcd = 'b1000;//8
#100 bcd = 'b1001;//9
#100 bcd = 'b1010;//a
#100 bcd = 'b1011;//b
#100 bcd = 'b1100;//c
#100 bcd = 'b1101;//d
#100 bcd = 'b1110;//e
#100 bcd = 'b1111;//f
#100

// Please make sure your stimulus is above this line 
$stop;
end 
