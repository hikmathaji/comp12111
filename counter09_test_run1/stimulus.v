
// Verilog stimulus file.
// Please do not create a module in this file.
/*

#VALUE      creates a delay of VALUE ns
a=VALUE;    sets the value of input 'a' to VALUE
$stop;      tells the simulator to stop

*/
initial 
	clock = 0;
	reset = 0;
always 
	begin 
	#100
	clock =~clock;
	end
	
initial
begin
#100 
// Enter you stimulus below this line
reset = 0;
#100
reset = 1;
#100
reset = 0;
#100

enable=1;
#1100


// Please make sure your stimulus is above this line 
$stop;
end 
