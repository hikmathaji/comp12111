
// Verilog stimulus file.
// Please do not create a module in this file.
/*

#VALUE      creates a delay of VALUE ns
a=VALUE;    sets the value of input 'a' to VALUE
$stop;      tells the simulator to stop

*/
initial 
	begin
	Clk = 0;
	Reset = 0;
	end
	
always
	begin
	#50
	Clk = ~Clk;
	end
	
initial
begin
#5000 
// Enter you stimulus below this line
Reset = 1;
#100
Reset = 0;

// Please make sure your stimulus is above this line 
$stop;
end 
